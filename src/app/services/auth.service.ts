import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of, from } from 'rxjs';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { filter } from 'minimatch';


const TOKEN_KEY = 'user-access-token';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  user: Observable<any>;
  private authState = new BehaviorSubject(null);

  constructor(private storage: Storage, private router: Router) {
    this.loadUser();
    this.user = this.authState.asObservable();

  }
  loadUser() {
    this.storage.get(TOKEN_KEY).then(data => {
      if (data) {
        this.authState.next(data);
      } else {
        this.authState.next({ email: null, role: null });
      }
    });
  }

  get userStorage() {
    let value = '';
    this.storage.get('TOKEN_KEY').then(res => {
      value = res;
      console.log(res);
    });
    console.log('value', value);
    return this.storage.get('TOKEN_KEY');
  }
  signIn(credentials): Observable<any> {
    const email = credentials.email;
    const pw = credentials.pw;
    let user = null;
    if (email === 'aravan27' && pw === 'osharavan') {
      user = { email, role: 'ADMIN' };
    } else if (email === 'aravan28' && pw === 'aravanosh') {
      user = { email, role: 'ADMIN' };
    } else if (email === 'akhiko' && pw === 'khikosoft') {
      user = { email, role: 'ADMIN' };
    } else if (email === '' || pw === '') {
      user = { email, role: 'NOT' };
    } else {
      user = { email, role: 'NOT' };
    }

    this.authState.next('isloggedin');
    this.storage.set('TOKEN_KEY', 'isloggedin');
    return of(user);

  }

  async signOut() {
    await this.storage.set(TOKEN_KEY, null);
    this.authState.next(null);
    this.router.navigateByUrl('/home');

  }
}
