import { TestBed } from '@angular/core/testing';

import { ShareinfoService } from './shareinfo.service';

describe('ShareinfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShareinfoService = TestBed.get(ShareinfoService);
    expect(service).toBeTruthy();
  });
});
