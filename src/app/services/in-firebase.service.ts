
import { Injectable } from '@angular/core';
import { AngularFirestoreModule, AngularFirestoreCollection, DocumentReference, AngularFirestore } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase';

export interface Info {
  uid?: string;
  id?: string;
  price: string;
  status: string;
  nameOf: string;
  name: string;
  phoneNumber: string;
  whatsApp: string;
  time: string;
  description: string;
  images: string[];
  key: string;
  downloadURLs: string[];
}

@Injectable({
  providedIn: 'root'
})
export class InFirebaseService {
  private info: Observable<Info[]>;
  private infoCollection: AngularFirestoreCollection<any>;


  constructor(private afs: AngularFirestore) {

    this.infoCollection = this.afs.collection<any>('news');
    
    this.info = this.infoCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      }),
      take(1)
    );
  }

  getData(): Observable<any[]> {
    return this.infoCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((a) => {
          const data = a.payload.doc.data() as Info; // TODO: check here
          return {uid: a.payload.doc.id, ...data};
        });
      })
    );
  }


  // uploadImage(imageURI): Promise<any> {

  //   return new Promise((resolve, reject) => {
  //     let storageRef = firebase.storage().ref();
  //     let imageRef = storageRef.child('image').child(`${new Date().getTime()}`);

  //     return this.encodeImageUri(imageURI, function (image64) {
  //       imageRef.putString(image64, 'data_url')
  //         .then(_ => {
  //           return imageRef.getDownloadURL().then(res => {
  //             console.log('URL: ', res);
  //             return res;
  //           });


  //         }, err => {
  //           reject(err);
  //         })
  //     })
  //   })
  // }

  // encodeImageUri(imageUri, callback) {
  //   var c = document.createElement('canvas');
  //   var ctx = c.getContext("2d");
  //   var img = new Image();
  //   img.onload = function () {
  //     var aux: any = this;
  //     c.width = aux.width;
  //     c.height = aux.height;
  //     ctx.drawImage(img, 0, 0);
  //     var dataURL = c.toDataURL("image/jpeg");
  //     callback(dataURL);
  //   };
  //   img.src = imageUri;
  // };

  getInfo() {
    return this.info;
  }


  getSingleInfo(id: string): Observable<Info> {
    return this.infoCollection.doc<Info>(id).valueChanges().pipe(
      take(1),
      map(inf => {
        inf.id = id;
        return inf;
      })
    );
  }

  addInfo(inf: Info): Promise<DocumentReference> {
    console.log('add to firebase', inf);
    return this.infoCollection.add(inf);
  }


  updateInfo(inf: Info): Promise<void> {
    return this.infoCollection.doc(inf.id).update({
      name: inf.name,
      phoneNumber: inf.phoneNumber,
      whatsApp: inf.whatsApp,
      description: inf.description,
      id: inf.id,
      price: inf.price,
      status: inf.status,
      nameOf: inf.nameOf,
      time: inf.time,
      images: inf.images,
      key: inf.key,
      downloadURLs: inf.downloadURLs
    });

  }

  deleteInfo(id: string): Promise<void> {
    return this.infoCollection.doc(id).delete();
  }
}
