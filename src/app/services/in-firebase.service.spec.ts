import { TestBed } from '@angular/core/testing';

import { InFirebaseService } from './in-firebase.service';

describe('InFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InFirebaseService = TestBed.get(InFirebaseService);
    expect(service).toBeTruthy();
  });
});
