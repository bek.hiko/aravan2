import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShareinfoService {
  openInSingle: any
  constructor() { }

  getAll(): Observable<any>{
    return this.openInSingle;
  }

  addAll(inf){
    this.openInSingle = inf;
  }
}
