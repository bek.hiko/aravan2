import { ShareinfoService } from './../services/shareinfo.service';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Router } from '@angular/router';
import { ShowSingleInfoPage } from './../show-single-info/show-single-info.page';
import { async } from '@angular/core/testing';
import { Storage } from '@ionic/storage';

import { Component, Renderer2, Input } from '@angular/core';
import { Info } from 'src/app/services/in-firebase.service';
import { InFirebaseService } from './../services/in-firebase.service';
import { Observable } from 'rxjs';
import { AuthService } from './../services/auth.service';
import { ModalController, Platform } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { AlertController } from '@ionic/angular';
import { ThrowStmt } from '@angular/compiler';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})

export class HomePage {
openInModalInfo = [[], []];
  constructor(
    private infoService: InFirebaseService,
    private auth: AuthService,
    private storage: Storage,
    private callNumber: CallNumber,
    private modalController: ModalController,
    private alertController: AlertController,
    private renderer: Renderer2,
    private router: Router,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private shareSer: ShareinfoService
  ) {
    this.countPress = 0;

    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.statusBar.backgroundColorByName('white');
    });
  }

  countPress: number;
  isSearchbarOpened = false;
  // Optional parameters to pass to the swiper instance. See http://idangero.us/swiper/api/ for valid options.
  SlideOpts = {
    initialSlide: 1,
    speed: 400,
    spaceBetween: 100
  };


  public loadedGoalList: Observable<Info[]>;
  bb = false;

  user = {
    email: '',
    pw: ''
  };

  allNews: Array<any> = [];
  searchResults: Array<any> = [];
  showSearchResult = false;

  telWhat = false;
  checkRole = false;
  public inf: Info[] = [];

  @Input('header') header: any;
    subscription: any;

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    this.infoService.getData().subscribe(data => {
      this.inf = data.sort((a: Info, b: Info) => {
        // @ts-ignore
        return new Date(b.time) - new Date(a.time);
      });
      this.openInModalInfo[1] = this.inf.slice(0, 15);
    });
    this.check();
    // this.filterList();
  }
    ionViewDidEnter() {
      this.subscription = this.platform.backButton.subscribe(() => {
          navigator['app'].exitApp();
      });
  }

  // ionViewWillLeave(){
  //     this.subscription.unsubscribe();
  // }



  initializeItems(): void {
  }

  delInfo(item) {
    console.log(item)
    this.infoService.deleteInfo(item.uid);
  }

  updateInfo(item) {
    this.infoService.updateInfo(item.uid);
    console.log('Working');
  }

  check() {
    this.storage.get('role').then((val) => {
      if (val == 'ADMIN') {
        console.log('You are Admin');
        this.checkRole = true;
      } else {
        console.log('You are not Admin');
      }
    });
  }


  async showSingleInfo(id) {
    this.bb = true;
    this.openInModalInfo[0] = id;
    const modal = await this.modalController.create({
      component: ShowSingleInfoPage,
      componentProps: {
        user_id: this.openInModalInfo
      }
    });
    modal.present();

  }

  showInfo(info){
    this.openInModalInfo[0] = info;
    this.shareSer.addAll(this.openInModalInfo);
    this.router.navigateByUrl(`/show-detailed/${info.name}`);
  }

  callNow(number) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  async presentAlertConfirm(item) {
    const alert = await this.alertController.create({
      header: 'Удалить Рекламу',
      message: 'При нажатия "ОК" реклама будет без возвратно удален с БазуДанных.',
      buttons: ['Отмена',
        {
          text: 'Удалить',
          handler: () => {
            this.delInfo(item);
          }
        }
      ]
    });

    await alert.present();
  }

  countPres() {
    this.countPress++;
  }

  allowStatus(status) {
    if (status == 'premium') {
      return true;
    }
  }

  onSearch(value) {
    console.log('value: ', value);
    const searchResult = this.inf.filter((inf) => {
      return inf.name.toLowerCase().includes(value.toLowerCase()) || value.toLowerCase().includes(inf.name.toLowerCase());
    });
    this.searchResults = searchResult;
    this.showSearchResult = true;
    if (!value.length) {
      this.showSearchResult = false;
    }
  }

  searchCancelled() {
    this.isSearchbarOpened = false;
    this.showSearchResult = false;
    console.log('clicked cancel');
    window.location.reload();
  }

  // filterList() {
  //   this.initializeItems();
  //   const _this = this;
  //   this.inf.subscribe({
  //     next(inf) {
  //       inf.forEach((info) => {
  //         _this.allNews.push(info);
  //       });
  //     }
  //   });

  // }


}

