import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { ShareinfoService } from './../services/shareinfo.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InFirebaseService } from './../services/in-firebase.service';
import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Info } from 'src/app/services/in-firebase.service';

@Component({
  selector: 'app-show-single',
  templateUrl: './show-single.component.html',
  styleUrls: ['./show-single.component.scss'],
})
export class ShowSingleComponent implements OnInit, OnDestroy {
  checkRole = false;
  myIcon = 'assets/telegram.png';
  allinf: Observable<any>;
  inf: Info;
  lastAddedAds: Info[] = [];
  subParams: Subscription;

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  openInModalInfo = [[], []];
  constructor(
    private infoService: InFirebaseService,
    private callNumber: CallNumber,
    private shareSer: ShareinfoService,
    private alertController: AlertController,
    private storage: Storage,
    private router: Router,
    private zone: NgZone,
    private route: ActivatedRoute

  ) { }

  userId = null;

  ngOnInit() {
    this.subParams = this.route.params.subscribe(params => {if (params) {const p = params['id'] }} );
    this.allinf = this.shareSer.getAll();
    this.inf = this.allinf[0];
    console.log(this.inf);
    this.openInModalInfo[1] = this.allinf[1].slice(0, this.allinf[1].length);
    this.openInModalInfo[1].push(this.inf);
    this.lastAddedAds.push(...this.allinf[1]);
    // console.log(this.lastAddedAds)
  }

  ngOnDestroy() {
    this.subParams.unsubscribe();
    }
  directCall(number) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  allowStatus(status) {
    if (status === 'premium') {
      return true;
    }
  }

  async presentAlertConfirm(item) {
    const alert = await this.alertController.create({
      header: 'Удалить Рекламу',
      message: 'При нажатия "ОК" реклама будет без возвратно удален с БазуДанных.',
      buttons: ['Отмена',
        {
          text: 'Удалить',
          handler: () => {
            this.delInfo(item);
          }
        }
      ]
    });

    await alert.present();
  }

  delInfo(item) {
    this.infoService.deleteInfo(item.id);
  }

  check() {
    this.storage.get('role').then((val) => {
      if (val === 'ADMIN') {
        this.checkRole = true;
      } else {
      }
    });
  }

  showInfo(info) {
    this.openInModalInfo[0] = info;
    this.shareSer.addAll(this.openInModalInfo);
    console.log(this.openInModalInfo);
    this.router.navigateByUrl(`/show-detailed1/${info.name}`);
  }
}
