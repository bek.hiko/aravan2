import { ShareinfoService } from './../services/shareinfo.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InFirebaseService } from './../services/in-firebase.service';
import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Observable } from 'rxjs';
import {Info} from 'src/app/services/in-firebase.service';




@Component({
  selector: 'app-show-single-info',
  templateUrl: './show-single-info.page.html',
  styleUrls: ['./show-single-info.page.scss'],
})
export class ShowSingleInfoPage implements OnInit {
  myIcon = 'assets/telegram.png';
  allinf = [];
  inf: Info;
  lastAddedAds:Info[] = [];
    slideOpts = {
      initialSlide: 1,
      speed: 400
    };

  constructor(
    private modalController: ModalController,
    private infoService: InFirebaseService,
    private navParams: NavParams,
    private callNumber: CallNumber,
    private shareSer: ShareinfoService
  ) { }

  userId = null;

  ngOnInit() {
    // this.allinf = this.navParams.get('user_id');
    // this.inf = this.allinf[0];
    // console.log(this.inf);
    // this.lastAddedAds.push(...this.allinf[1]);
    // console.log(this.lastAddedAds)
    console.log(    this.shareSer.getAll()
    )
  }

  directCall(number) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  closeModal() {
      this.modalController.dismiss();
  }

}
