
import { AngularFirestore } from '@angular/fire/firestore';
import { Platform } from '@ionic/angular';
import { Injectable, OnInit } from '@angular/core';
import {Firebase} from '@ionic-native/firebase/ngx';


@Injectable({
  providedIn: 'root'
})
export class FcmService {

  constructor(
    public firebaseNative: Firebase,
    public afs: AngularFirestore,
    private platform: Platform,
    
  ) { 
    
  }

  ngOnInit(){
    this.getToken();
  }
  token: string;

  async getToken(){
  
    this.token = await  this.firebaseNative.getToken();
    

      //this.token = this.firebaseNative.getToken();
      console.log("I got Token",this.token);
    

    return this.saveTokenToFirestore(this.token);
  }

  saveTokenToFirestore(token){
    if(!token) return;
    const deviceRef =this.afs.collection('devices')

    const docData ={
      uid: token,
      token,
      userId: 'user-'+new Date().toISOString()
    };
    return deviceRef.doc(token).set(docData, {merge: true});
  }

  listenNotifications(){
    return this.firebaseNative.onNotificationOpen();
  }


}
