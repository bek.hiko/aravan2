import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowSingle1Component } from './show-single1.component';

describe('ShowSingle1Component', () => {
  let component: ShowSingle1Component;
  let fixture: ComponentFixture<ShowSingle1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowSingle1Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowSingle1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
