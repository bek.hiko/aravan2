import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { ShareinfoService } from './../services/shareinfo.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InFirebaseService } from './../services/in-firebase.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {Info} from 'src/app/services/in-firebase.service';

@Component({
  selector: 'app-show-single1',
  templateUrl: './show-single1.component.html',
  styleUrls: ['./show-single1.component.scss'],
})
export class ShowSingle1Component implements OnInit {
  checkRole = false;
  myIcon = 'assets/telegram.png';
  allinf: Observable<any>;
  inf: Info;
  lastAddedAds:Info[] = [];
    slideOpts = {
      initialSlide: 1,
      speed: 400
    };
    openInModalInfo = [[],[]];
  constructor(
    private infoService: InFirebaseService,
    private callNumber: CallNumber,
    private shareSer: ShareinfoService,
    private alertController: AlertController,
    private storage: Storage,
    private router: Router
  ) { }

  userId = null;

  ngOnInit() {
    this.allinf = this.shareSer.getAll();
    this.inf = this.allinf[0];
    console.log(this.inf);
    this.openInModalInfo[1] = this.allinf[1].slice(0, this.allinf[1].length);
    this.openInModalInfo[1].push(this.inf)
    this.lastAddedAds.push(...this.allinf[1]);
    // console.log(this.lastAddedAds)
  }

  directCall(number) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  allowStatus(status) {
    if (status === 'premium') {
      return true;
    }
  }

  async presentAlertConfirm(item) {
    const alert = await this.alertController.create({
      header: 'Удалить Рекламу',
      message: 'При нажатия "ОК" реклама будет без возвратно удален с БазуДанных.',
      buttons: ['Отмена',
        {
          text: 'Удалить',
          handler: () => {
            this.delInfo(item);
          }
        }
      ]
    });

    await alert.present();
  }

  delInfo(item) {
    this.infoService.deleteInfo(item.id);
  }

  check() {
    this.storage.get('role').then((val) => {
      if (val === 'ADMIN') {
      this.checkRole = true;
      } else {
      }
    });
  }

  showInfo(info) {
    this.openInModalInfo[0] = info;
    // this.openInModalInfo[1] = this.openInModalInfo[1].filter(x => x.id !== info.id);
    this.shareSer.addAll(this.openInModalInfo);
    console.log(this.openInModalInfo);
    this.router.navigateByUrl(`/show-detailed/${info.name}`);
    }
}
