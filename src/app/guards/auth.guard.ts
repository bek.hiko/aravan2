import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthService, private alertCtrl: AlertController) { }

  async canActivate(route: ActivatedRouteSnapshot) {
    // return this.auth.user.pipe(
    //   take(1),
    //   map(user => {
    //     console.log('log: ', user);
    //     if (user) {
    //       return true;
    //     } else {
    //       this.showAlert();
    //       return this.router.parseUrl('/login');
    //     }

    //   })
    // )
    let a = ''
    await this.auth.userStorage.then(res =>{
       a = res
    })
    console.log(a)
    if ( a == 'isloggedin') {
      return true;
    } else {
      this.showAlert();
      return this.router.parseUrl('/login');
    }
  }
  async showAlert() {
    let alert = await this.alertCtrl.create({
      header: 'UNAUTHORIZED',
      message: 'You are not logged in',
      buttons: ['OK']
    })
    alert.present();
  }


}
