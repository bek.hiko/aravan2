import { AuthService } from './../services/auth.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  user = {
    email: '',
    pw: ''
  };

  constructor(private auth: AuthService,
    private router: Router,
    private alertCtrl: AlertController,
    private storage: Storage) { }

  ngOnInit() {
  }

  signIn() {
    this.auth.signIn(this.user).subscribe(user => {
      let role = user['role'];
      if (role == 'ADMIN') {
        this.router.navigateByUrl('/admin');
        this.storage.set("role", "ADMIN");
      } else if (role == 'NOT' || role == null) {
        this.shAlert();
      }
    });

  }
  cancel() {
    this.router.navigateByUrl('/');
  }

  async shAlert() {
    let alert = await this.alertCtrl.create({
      header: 'UNAUTHORIZED',
      message: 'Не правильный пароль или логин',
      buttons: ['OK']
    })
    alert.present();
  }

}
