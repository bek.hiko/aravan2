import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';

import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { Info } from 'src/app/services/in-firebase.service';
import { ActivatedRoute, Router } from '@angular/router';
import { InFirebaseService } from './../services/in-firebase.service';
import { Storage } from '@ionic/storage';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AngularFireStorage, AngularFireUploadTask, AngularFireStorageModule } from '@angular/fire/storage';
import { from } from 'rxjs';
import * as firebase from 'firebase';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {
  images = [];
  editConfirmed = false;
  constructor(private auth: AuthService,
              public imagePicker: ImagePicker,
              public file: File,
              private activetedRoute: ActivatedRoute,
              private infoService: InFirebaseService,
              private router: Router,
              private storage: Storage,
              private webView: WebView,
              private angStorage: AngularFireStorage,
              private alertController: AlertController
  ) { }

  // typeOfAd :string ="n64";

  inf: Info = {
    price: '7000 USD',
    status: 'classic',
    nameOf: 'Алтын',
    name: 'BMW 5 series 3 л.',
    phoneNumber: '+996 555187190',
    whatsApp: '+996 555187190',
    // tslint:disable-next-line: new-parens
    time: '',
    description: 'BMW 530i, E60, 2003 г.в,черный цвет,черный кожаный салон и много чего еще.',
    images: [],
    key: '',
    downloadURLs: []
  };

  selectedImages: any[] = [];

  image = [];


  goFish(event) {
    this.inf.status = event.target.value;
  }

  ngOnInit() {
    const id = this.activetedRoute.snapshot.paramMap.get('id');
    if (id) {
      this.editConfirmed = true;
      this.infoService.getSingleInfo(id).subscribe(inf => {
        this.inf = inf;
        console.log(this.inf);
      });

    }

  }
  signOut() {
    this.auth.signOut();
    this.storage.remove('role');

  }

  async openImagePicker() {

    const permissionResult = await this.imagePicker.hasReadPermission();
    console.log('Permission result: ' + permissionResult);
    if (permissionResult === false) {
      // no callbacks required as this opens a popup which returns async
      await this.imagePicker.requestReadPermission();
    }

    if (permissionResult === true) {
      try {
        const selectedImages = await this.imagePicker.getPictures({
          maximumImagesCount: 8
        });
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < selectedImages.length; i++) {
          // console.log(this.selectedImages[i]);
          this.images.push(this.webView.convertFileSrc(selectedImages[i]))
        }
        console.log('Selected images: ' + selectedImages.length);
        console.log(selectedImages);
        this.selectedImages = selectedImages;
      } catch (err) {
        console.log('Can not choose image');
        console.log(err);
      }

    }
    // console.log('FINISHED');
    console.log(this.inf);
  }

  encodeImageUri(imageUri): Promise<string> {
    return new Promise((resolve, reject) => {
      let c = document.createElement('canvas');
      let ctx = c.getContext('2d');
      let img = new Image();
      img.onload = function() {
        let aux: any = this;
        c.width = aux.width;
        c.height = aux.height;
        ctx.drawImage(img, 0, 0);
        let dataURL = c.toDataURL('image/jpeg');
        resolve(dataURL);
      };
      img.src = imageUri;
    });
  }


  async addInfo() {
    this.presentAlertConfirm();

    for (let i = 0; i < this.selectedImages.length; i++) {
      console.log(this.selectedImages[i]);
      const imageUrlFirst = this.webView.convertFileSrc(this.selectedImages[i]);
      // console.log(imageUrlFirst);
      const imageUrl = await this.encodeImageUri(imageUrlFirst);
      const storageRef = firebase.storage().ref();
      const imageRef = storageRef.child('image').child(`${new Date().getTime()}`);
      const uploadRes = await imageRef.putString(imageUrl, 'data_url');
      // console.log('uploaded image');
      // console.log(uploadRes);
      // console.log(imageRef.getDownloadURL);
      const downloadimageUrl = await imageRef.getDownloadURL();
      // console.log('retrieved image url');
      // console.log(downloadimageUrl);
      this.inf.images.push(downloadimageUrl);

    }
    console.log(this.inf)
    this.infoService.addInfo(this.inf).then(() => {
      this.router.navigateByUrl('/');
      // this.showToast('Инфо добавлена');

    }, err => {
      // this.showToast('возникла проблема');
      console.log(err);
    });
  }
  deleteInfo() {
    this.infoService.deleteInfo(this.inf.id).then(() => {
      this.router.navigateByUrl('/');
      // this.showToast('Succsess');

    }, err => {
      // this.showToast('возникла проблема');
      console.log(err);
    });
  }

  updateInfo() {
    this.infoService.updateInfo(this.inf).then(() => {
      this.router.navigateByUrl('/');
      // this.showToast('succsess');

    }, err => {
      // this.showToast('возникла проблема');
      console.log(err);
    });
  }

  cancel() {
    this.router.navigateByUrl('/');
  }




  //   this.task.then((response) => {
  //       this.angStorage.ref(path).getDownloadURL().toPromise().then((data) => {
  //           this.inf.downloadURLs.push(data);
  //           this.inf.images.push(path);
  //       });
  //   });
  // }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Подаждите пока опубликуется реклама',
      message: 'Пожалуйста подаждите поке не загрузка рекламы. При нажатия несколько раз в кнопку "Опубликовать" реклама будет опубликовано несколько раз. После того как закончитсья загрузка программа автоматически переводит вас в главную страницу.',
      buttons: ['Отмена'
        //  {   text: 'Удалить',
        //     handler: () => {
        //       this.delInfo(item);
        //     }
        //   }
      ]
    });

    await alert.present();
  }

}
