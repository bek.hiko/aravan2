import { ShowSingle1Component } from './show-single1/show-single1.component';
import { ShowSingleComponent } from './show-single/show-single.component';
import { AuthGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'admin', loadChildren: './admin/admin.module#AdminPageModule', canActivate: [AuthGuard]},
  { path: 'edit/:id', loadChildren: './admin/admin.module#AdminPageModule', canActivate: [AuthGuard]},
  { path: 'user', loadChildren: './user/user.module#UserPageModule' },
  { path: 'into', loadChildren: './into/into.module#IntoPageModule' },
  // loadChildren:'./show-single-info/show-single-info.module#ShowSingleInfoPageModule' 
  {path: 'show-detailed/:id', component: ShowSingleComponent },
  {path: 'show-detailed1/:id', component: ShowSingle1Component }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
