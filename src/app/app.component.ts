import { Router } from '@angular/router';
import { Component, QueryList, ViewChildren } from '@angular/core';

import { Platform, ModalController, ActionSheetController, MenuController, IonRouterOutlet, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { FcmService } from './sercices/fcm.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private socialShare: SocialSharing,
    private router: Router,
    public modalCtrl: ModalController,
    private menu: MenuController,
    private fcmService: FcmService,
    private actionSheetCtrl: ActionSheetController,


  ) {
    this.initializeApp();
    this.backButtonEvent();

    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.statusBar.backgroundColorByName('white');
    });
  }
  public appPages = [
    {
      title: 'Новости',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Биздин Максат',
      url: '/into',
      icon: 'contacts'
    }
  ];

  lastTimeBackPress = 0;
  timePeriodToExit = 500;

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  text = 'Араван Жapнак мобилдик тиркемеси. Кочуруп алуу учун Google Play ден Араван Жарнак деп издениз.';
  url = 'https://play.google.com/store/apps/details?id=io.flaterlab.aravanjarnak&hl=en';
  telegram = 'com.social.telegram';

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.fcmService.getToken().then((data) => {
        console.log('getToken result');
        console.log(data);
      });
    });
  }
  instaShare() {
    this.socialShare.shareViaInstagram(this.text, null).then((res) => {
      console.log('Success');
    }).catch((e) => {
      console.log(e);
    });
  }
  whatShare() {
    this.socialShare.shareViaWhatsApp(this.text, this.url).then((res) => {
      console.log('Success');
    }).catch((e) => {
      console.log(e);
    });
  }
  faceShare() {
    this.socialShare.shareViaFacebook(this.text, null, this.url).then((res) => {
      console.log('Success');
    }).catch((e) => {
      console.log(e);
    });
  }

  telegramShare() {
    this.socialShare.shareVia(this.telegram, this.text);
  }


  openLoginPage() {
    this.router.navigateByUrl('/admin');
  }

  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      // close action sheet
      try {
        const element = await this.actionSheetCtrl.getTop();
        if (element) {
          console.log('Im working well');
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close popover
      // try {
      //     const element = await this.popoverCtrl.getTop();
      //     if (element) {
      //         element.dismiss();
      //         return;
      //     }
      // } catch (error) {
      // }

      // close modal
      try {
        const element = await this.modalCtrl.getTop();
        if (element) {
          console.log('Im working well');
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }

      // close side menua
      try {
        const element = await this.menu.getOpen();

        if (element !== null) {
          if (this.router.url === '/home') {
            console.log('we are at home', this.router.url);

            // this.platform.exitApp(); // Exit from app
            console.log('Exit app wortks');
            navigator['app'].exitApp(); // work for ionic 4
          }

          console.log('Im working well menu');
          this.menu.close();
          return;

        }

      } catch (error) {

      }

      this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
        if (outlet && outlet.canGoBack()) {
          console.log('Im working well cangoback');
          outlet.pop();

        } else if (this.router.isActive('/home', true)) {
          console.log('we are at home');
          if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            // this.platform.exitApp(); // Exit from app
            console.log('Exit app wortks');
            navigator['app'].exitApp(); // work for ionic 4

          } else {
            console.log('Not working');
            // this.toast.show(
            //     `Press back again to exit App.`,
            //     '2000',
            //     'center')
            //     .subscribe(toast => {
            //         // console.log(JSON.stringify(toast));
            //     });
            this.lastTimeBackPress = new Date().getTime();
          }
        }
      });
    });
  }


}
