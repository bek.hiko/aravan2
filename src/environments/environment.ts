// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
      apiKey: 'AIzaSyBr5FoPJt6pbrN-6F5HthKoIBjcSf2nwNY',
      authDomain: 'arav-1c890.firebaseapp.com',
      databaseURL: 'https://arav-1c890.firebaseio.com',
      projectId: 'arav-1c890',
      storageBucket: 'arav-1c890.appspot.com',
      messagingSenderId: '474993992127',
      appId: '1:474993992127:web:0d0796eb041f7a66b1810a',
      measurementId: 'G-5X99NZQVKV'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
