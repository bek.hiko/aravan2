const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//     response.send("Hello from Firebase!");
// });

var admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

exports.newNewsCreated = functions.firestore
    .document('news/{newsId}')
    .onCreate((snap, context) => {
        const data = snap.data();
// Notification content
        const payload = {
            notification: {
                title: 'АРАВАН ЖАРНАК',
                body: `Тема: ${data.name}\n Описание: ${data.description}`,
                sound: 'default'
            }
        };
// // ref to the device collection for the user
        const db = admin.firestore();
        const devicesRef = db.collection('devices');
        return devicesRef.get()
            .then((querySnapshot) => {
                const tokens = [];
                querySnapshot.forEach(doc => {
                    const user = doc.data();
                    tokens.push(user.token);
                });

                console.log("Received tokens");
                console.log(tokens.length);
                console.log(tokens);
                return admin.messaging().sendToDevice(tokens, payload)
            }).catch((err) => {
                return Promise.reject(err);
            });
    });
